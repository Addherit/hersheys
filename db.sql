
CREATE TABLE users (
    id int AUTO_INCREMENT,
    username varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
	nivel int NOT NULL,
	PRIMARY KEY(id)
);

INSERT INTO users(username,password,nivel) VALUES ("admin","1234",1);
INSERT INTO users(username,password,nivel) VALUES ("erni","1234",2);
INSERT INTO users(username,password,nivel) VALUES ("hsy","123",2);

CREATE TABLE info (
    id int AUTO_INCREMENT,
    fecha varchar(255) NOT NULL,
    clave varchar(255) NOT NULL,
	name varchar(255) NOT NULL,
    status varchar(255) NOT NULL,
    centroc int NOT NULL,
	descentro varchar(255) NOT NULL,
    descsucursal varchar(255) NOT NULL,
    tnom varchar(255) NOT NULL,
	fechaingre varchar(255) NOT NULL,
    antiguedad varchar(255) NOT NULL,
	badge varchar(255) NOT NULL,
	imagen varchar(255) NOT NULL,
	PRIMARY KEY(id)
);

INSERT INTO info (fecha,clave,name,status,centroc,descentro,descsucursal,tnom,fechaingre,antiguedad,badge,imagen) 
VALUES ("43465","68807","JASSO ZAVALA TOMAS DANIELl","P",5007,"MAINTENANCE","EL SALTO PLANT","QUI","29409","38 años, 5 meses","2310026386"," ");


CREATE TABLE asistencia (
    id int AUTO_INCREMENT,
    idinfo int NOT NULL,
	premio int,
	PRIMARY KEY(id),
	FOREIGN KEY (idinfo) REFERENCES info(id)
);

CREATE TABLE premios (
    id int AUTO_INCREMENT,
	premio int,
	PRIMARY KEY(id)
);
