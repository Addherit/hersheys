<?php
include_once "session.php";

if($_SESSION['nivel'] == 2){
	header("location: enterC.php");
}
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Hersheys - Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="assets/images/icon/favicon.ico">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/metisMenu.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/slicknav.min.css">
    <!-- others css -->
    <link rel="stylesheet" href="assets/css/typography.css">
    <link rel="stylesheet" href="assets/css/default-css.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body class="body-bg">
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
        <!-- main header area end -->
        <!-- header area start -->
        <div class="header-area header-bottom">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-9  d-none d-lg-block">
                        <div class="horizontal-menu">
                            <nav>
                                <ul id="nav_menu">
                                    <li class="mega-menu">
                                        <a href="enterC.php"><i class="ti-layers-alt"></i> <span>Entrada</span></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><i class="fa fa-table"></i>
                                            <span>Reportes</span></a>
                                        <ul class="submenu">
											<li><a href="/hershey/reports/report-asis.php">Asistencia</a></li>
                                            <li><a href="/hershey/reports/report-perma.php">Permanentes</a></li>
                                            <li><a href="/hershey/reports/report-event.php">Eventuales</a></li>
                                            <li><a href="/hershey/reports/report-time-5.php">Antiguedad 5</a></li>
                                            <li><a href="/hershey/reports/report-time-10.php">Antiguedad 10</a></li>
                                            <li><a href="/hershey/reports/report-time-15.php">Antiguedad 15</a></li>
                                            <li><a href="/hershey/reports/report-time.php">Antiguedad 20+</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!-- nav and profile section -->
                        <div class="clearfix d-md-inline-block d-block">
                            <div class="user-profile m-0">
                                <img class="avatar user-thumb" src="assets/images/author/dog.png" alt="avatar">
								<h4 class="user-name dropdown-toggle" data-toggle="dropdown">
									<?php echo $_SESSION['login_user'];?>
								<i class="fa fa-angle-down"></i></h4>

                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Settings</a>
                                    <a class="dropdown-item" href="logout.php">Log Out</a>
                                </div>
                            </div>
                        </div>
                    <!-- mobile_menu -->
                    <div class="col-12 d-block d-lg-none">
                        <div id="mobile_menu"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- header area end -->
        <!-- page title area end -->
<br>
        <div class="main-content-inner">
                <div class="row">

					<div class="col">
						<div class="col-12 mt-5">
							<div class="card">
								<div class="card-body">
									<?php include "upload.php";?>

								</div>
							</div>
						</div>
					</div>
<!--					<div class="col">
						<div class="col-12 mt-5">
							<div class="card">
								<div class="card-body">
									<?php include "premio.php";?>

								</div>
							</div>
						</div>
					</div>-->

					<div class="w-100"></div>

					<div class="col">
						<!-- Dark table start -->
						<div class="col-12 mt-5">
							<div class="card">
								<div class="card-body">
									<h4 class="header-title">Datos Ingresados</h4>
									<div class="data-tables datatable-dark">
									<?php
									require_once 'simplexlsx.class.php';

									if ( $xlsx = SimpleXLSX::parse('uploads/test01.xlsx')) {
										echo("<table id='dataTable3' class='text-center table'>");
										echo("<thead class='text-capitalize'>");
										list( $cols,$row ) = $xlsx->dimension();
										foreach ( $xlsx->rows() as $k => $r ) if($e < $row){
											echo '<tr>';
											for ( $i = 0; $i < $cols; $i ++ ) {
												echo '<td>'.$r[ $i ].'</td>';
											}
											echo '</tr>';
											if ($k == 0) {
												echo("</thead>");
												echo("<tbody>");
											}
											$e++;
										}
										echo("</tbody>");
										echo '</table>';
									}
									?>

									</div>
								</div>
							</div>
						</div> <!-- Dark table end --> </div>




                </div>
        </div>
<body>
        <!-- main content area end -->
    </div>
    <!-- main wrapper start -->
    <!-- offset area end -->
    <!-- jquery latest version -->
    <script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
    <!-- bootstrap 4 js -->
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/metisMenu.min.js"></script>
    <script src="assets/js/jquery.slimscroll.min.js"></script>
    <script src="assets/js/jquery.slicknav.min.js"></script>

    <!-- others plugins -->
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/scripts.js"></script>
</body>

</html>
